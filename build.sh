#!/bin/bash

if [[ $1 == "start" ]];
	then

    mkdir  static/beaches/images
    mkdir  static/museums/images
    mkdir  static/worthseeings/images
    mkdir  static/directions/images

    export WP_SOURCE_TERMID=$2
    export WP_SOURCE_LANG=$3

    node index.js

elif [[ $1 == "push" ]];
    then

    export WP_DESTINATION=$2
    export WP_DESTINATION_USERNAME=$3
    export WP_DESTINATION_PASSWORD=$4
    node push.js

    echo "$1 environment created"

elif [[ $1 == "end" ]];
	then
	rm -rf static/beaches/images
	rm -rf static/beaches/WPdata.json
	rm -rf static/museums/images
	rm -rf static/museums/WPdata.json
    rm -rf static/worthseeings/images
    rm -rf static/worthseeings/WPdata.json
    rm -rf static/directions/images
    rm -rf static/directions/WPdata.json

    echo "$1 environment created"

else
echo "Parameter not right."
fi

