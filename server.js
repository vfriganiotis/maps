const express = require('express')
const bodyParser = require('body-parser')
const server = express()
const port = 3000

const maps=require('./maps/routes/maps')

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json()); // support json encoded bodies

server.use('/maps',maps)

server.listen(port, () => console.log(`Example app listening on port ${port}!`))