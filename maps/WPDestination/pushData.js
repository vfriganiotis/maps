const fs =require('fs');
const Headers = require('./authentication')
const Request = require('./WPrequest')

async function PushData(DATA,termId,ObjTermsIds,wpDestination){

    //WP
    let WPMaps = 'https://' + wpDestination + '/wp-json/wp/v2/maps',
        WPImages ='https://' + wpDestination + '/wp-json/wp/v2/media/';

    let count = 0;
    let galleryCount = 0

    for ( const item of DATA.data){

        //Get Count i
        DATA.count = count;
        item.galleryCount = []
        item.featureImage = false;
        //Check if Image is not empty

        if( item.img !== '' ){

            let formData = {
                file: [fs.createReadStream(item.img)],
            };

            let WPMediaPost = {
                url : WPImages,
                headers : Headers.WPHeaders.WPMediaHeaders,
                method: "POST",
                formData: formData
            };

            console.log(formData)
            const w = await Request.init(WPMediaPost,DATA,ObjTermsIds)

            //increment count for DataMuseumsRaw.data[DataPagesRaw.count] after wp create image id

        }

        item.featureImage = true

        if( item.gallery !== null ){

            for( let galleryItem of item.gallery){

                let formData = {
                    file: [fs.createReadStream(galleryItem)],
                };

                let WPMediaPost = {
                    url : WPImages,
                    headers : Headers.WPHeaders.WPMediaHeaders,
                    method: "POST",
                    formData: formData
                };

                const w = await Request.init(WPMediaPost,DATA,ObjTermsIds)

            }

        }

        count++

    }

    console.log(DATA.data)

    for (const item of DATA.data) {

        //if empty IMG send value -1
        if( item.img === ""){
            item.img = -1
        }

        const JsonBody = {
            "title" : "Jedi",
            "status" : "publish"
        }

        JsonBody.title = item.title;
        JsonBody.content = item.content;
        JsonBody.featured_media = item.img;
        JsonBody.fields = {
            lat : item.lat,
            lng : item.lng,
            gallery: item.galleryCount
        }

        JsonBody.sightseeings = termId;

        const WPCreatePost = {
            url : WPMaps,
            headers : Headers.WPHeaders.headers,
            method: "POST",
            body: JsonBody,
            json: true
        }

        const v = await Request.init(WPCreatePost,DATA,ObjTermsIds)

    }
}

exports.push = PushData