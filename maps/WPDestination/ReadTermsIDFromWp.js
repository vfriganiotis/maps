const Request = require('./WPrequest')
const Headers = require('./authentication')

//WP
let WPterms =  'https://'+ process.env.WP_DESTINATION +'/wp-json/wp/v2/sightseeings/';

async function readTermIDFromWP(callback,objTerms) {

    const WP = {
        url: WPterms,
        Headers,
        method: "GET"
    }

    const ReadTerm = await Request.init(WP, '', objTerms)

    //Run the Async request
    callback()
    console.log(1)
}

exports.call = readTermIDFromWP
