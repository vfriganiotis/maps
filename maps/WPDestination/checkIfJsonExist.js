const fs =require('fs');
let path = require("path");
let reqPath = path.join(__dirname, '../');

function checkIfExistJson(PushData,ObjTermsIds,domain){
    try {
        if (fs.existsSync(reqPath + '/static/museums/WPdata.json')) {
            let  DataMuseumsRaw = require('../static/museums/WPdata');
            if( DataMuseumsRaw.data.length > 0){
                PushData(DataMuseumsRaw,ObjTermsIds.museums,ObjTermsIds,domain)
            }
        }
    } catch(err) {
        console.error(err)
    }

    try {
        if (fs.existsSync(reqPath +'/static/beaches/WPdata.json')) {
            let DataBeachesRaw  = require('../static/beaches/WPdata');
            if(DataBeachesRaw.data.length > 0){
                PushData(DataBeachesRaw ,ObjTermsIds.beaches,ObjTermsIds,domain)
            }
        }
    } catch(err) {
        console.error(err)
    }

    try {
        if (fs.existsSync(reqPath + '/static/worthseeings/WPdata.json')) {
            let DataDirectionsRaw = require('../static/worthseeings/WPdata.json');
            if( DataDirectionsRaw.data.length > 0 ){
                PushData(DataDirectionsRaw,ObjTermsIds.worthseeings,ObjTermsIds,domain)
            }
        }
    } catch(err) {
        console.error(err)
    }
}

exports.checkJSON = checkIfExistJson