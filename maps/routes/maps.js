const express=require('express')
const router=express.Router()

const dataMap = require('../index');
const pushMap = require('../push');

const fs = require('fs');
const path = require('path');

router.get('/', function(req,res){

    const directory = ['maps/static/beaches/images','maps/static/museums/images',"maps/static/worthseeings/images"];

    for( let i = 0; i < directory.length; i++){
        let c = directory[i]
        fs.readdir(c, (err, files) => {
            if (err) throw err;

            for (const file of files) {
                fs.unlink(path.join(c, file), err => {
                    if (err) throw err;
                });
            }
        });
    }

    dataMap.init('en',4,res,pushMap,'greece-hotel.info/admins/test')

})


module.exports = router;
