const getData = require('./WPSource/init')
const Authentication = require('./WPSource/authentication')

function dataMaps(lang,WPplaceID,res,getmap,domain){

    const WPMuseums = "https://maps.sitesdemo.com/" + lang + "/wp-json/wp/v2/museums?per_page=100",
        WPBeaches = "https://maps.sitesdemo.com/" + lang + "/wp-json/wp/v2/beaches?per_page=100",
        WPSigthSeeings = "https://maps.sitesdemo.com/" + lang + "/wp-json/wp/v2/worthseeings?per_page=100";

    const dataRequest = [
        {
            WP: {
                url : WPMuseums,
                Authentication
            },
            slug: 'museums'
        },
        {
            WP: {
                url : WPBeaches,
                Authentication
            },
            slug: 'beaches'
        },
        {
            WP: {
                url : WPSigthSeeings,
                Authentication
            },
            slug: 'worthseeings'
        }
    ]

    getData.init(dataRequest,WPplaceID,res,getmap,domain)
}
exports.init = dataMaps;





















