const request = require('request');
const fs =require('fs');
const download = require('./download');
let info,map;
let cc = 0;
const REQUEST = function(url,postType,WPplaceIDs) {

    const WPplaceID = parseInt(WPplaceIDs);//term ID

    return new Promise((resolve, reject) => {
        request( url , function(error, response, body){
             console.log(response.statusCode)

            if (!error && response.statusCode === 200) {

                if( typeof body === 'string'){

                    let OUTPUT = {
                        data : []
                    }

                    info = JSON.parse(body)

                    info.map(function(item) {

                        let galleries = []
                        //return array with length 0, if can not find the term
                        let term = item.places.filter(function(place){
                            return place === WPplaceID
                        })

                        //Check if post exist in this term
                        if( term.length > 0 ){

                            let split,url = '';

                            if( item.better_featured_image !== null ){

                                split = item.better_featured_image.source_url.split('/')
                                url = './maps/static/'+ postType + "/images/" + split[split.length-1];

                                download.init(item.better_featured_image.source_url, url, function(){
                                    console.log('done');
                                });

                            }

                            if( item.acf.hasOwnProperty('gallery') ){

                                let split,url = '';
                                if( item.acf.gallery !== null && item.acf.gallery !== false){


                                    item.acf.gallery.map(function(item){
                                        console.log(item.url)
                                        split = item.url.split('/')
                                        url = './maps/static/'+ postType + "/images/" + split[split.length-1] ;
                                        galleries.push(url)

                                        download.init(item, url, function(){
                                            console.log('done');
                                        });
                                    })

                                }
                            }

                            map = {
                                title : item.title.rendered,
                                content : item.content.rendered,
                                lat : item.acf.lat,
                                lng : item.acf.lng,
                                img : url,
                                gallery: galleries
                            }

                            OUTPUT.data.push(map)
                        }

                    })

                    let JsonUrl = './maps/static/' + postType + '/WPdata.json';

                    fs.writeFile( JsonUrl , JSON.stringify(OUTPUT) , 'utf8',(err)=> {
                        console.log("change data output")
                        resolve(cc++)
                        return (err) ? console.log(err): console.log("The file pages was saved!");
                    })

                }

            }else{
                console.log(error)
            }

        });

    });

}

exports.init = REQUEST;